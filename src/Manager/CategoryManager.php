<?php

namespace App\Manager;

use App\Entity\Category;

/**
 * Класс-менеджер для управления сущностями типа
 */
class CategoryManager
{
    public function createCategory(string $name)
    {
        $category = new Category();
        $category->setName($name);

        return $category->create();
    }
}
