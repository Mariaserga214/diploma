<?php

namespace App\Manager;

use App\Entity\User;

/**
 * Класс-менеджер, который умеет создавать/редактировать/удалять пользователей,
 * а также, при необходимости, выполнять другие действия над сущностью User
 *
 * Возможности в интерфейсе администратора
 * Просматривать список администраторов.
 * Создавать новых администраторов.
 * Изменять пароли существующих администраторов.
 * Удалять существующих администраторов.
 */
class UserManager
{
    public function createNewAdmin(string $email, string $name, string $password)
    {
        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setName($name);
        $user->setIsAdmin(true);

        return $user->create();
    }

    public function createNewUser(string $email, string $name, string $password)
    {
        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setName($name);
        $user->setIsAdmin(false);

        return $user->create();
    }

    public function changeAdminPassword(User $user, string $new_password)
    {
        $user->setPassword($new_password);

        return $user->update();
    }

    public function deleteAdmin(User $user)
    {
        $user->delete();
    }
}


//$user_manager = new UserManager();
//$user_manager->createNewAdmin('something@anything.ru', '123', 'Вячеслав');
