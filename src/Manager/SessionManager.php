<?php

namespace App\Manager;

use App\Entity\User;

class SessionManager
{
    public function __construct()
    {
        session_start();
    }

    public function loginUser(User $user)
    {
        $_SESSION['user'] = $user;
    }

    public function logoutUser()
    {
        unset($_SESSION['user']);

        session_destroy();
    }

    public function isUserLogged()
    {
        return array_key_exists('user', $_SESSION);
    }
}
