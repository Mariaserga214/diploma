<?php

namespace App\Manager;

use App\Entity\Category;
use App\Entity\Question;
use App\Entity\User;

/**
 * Класс-менеджер, который занимается управлением сущностей класса Question
 */
class QuestionManager
{
    public function createNewQuestion(string $name, string $email, int $category_id, string $text)
    {
        $user = (new User())->findByNameAndEmail($name, $email);
        $category = (new Category())->findById($category_id);

        $question = new Question();
        $question->setUser($user);
        $question->setCategory($category);
        $question->setText($text);
        $question->setStatus(Question::STATUS_WAITING);

        $question->create();

        return $question;
    }

    public function editQuestion(Question $question, User $user, Category $category, string $text, string $answer)
    {
        $question->setUser($user);
        $question->setCategory($category);
        $question->setText($text);
        $question->setAnswer($answer);

        $question->update();

        return $question;
    }

    public function answerToQuestion(Question $question, string $answer)
    {
        $question->setAnswer($answer);
        $question->setStatus(Question::STATUS_PUBLISHED);

        $question->update();
    }

    public function deleteQuestion(Question $question)
    {
        $question->delete();
    }

    public function publishQuestion(Question $question)
    {
        $question->setStatus(Question::STATUS_PUBLISHED);

        $question->update();
    }

    public function hideQuestion(Question $question)
    {
        $question->setStatus(Question::STATUS_HIDDEN);

        $question->update();
    }

    public function changeCategory(Question $question, Category $category)
    {
        $question->setCategory($category);

        $question->update();
    }
}
