<?php

namespace App\Manager;

use App\Controller\AdminCategoryController;
use App\Controller\AdminController;
use App\Controller\AdminQuestionController;
use App\Controller\IndexController;
use App\Controller\QuestionController;

class Router
{
    protected $routes = [];

    /**
     * @param string $route
     * @param array $params
     */
    public function addRoute(string $route, array $params)
    {
        $this->routes[$route] = $params;
    }

    /**
     * @param string $route
     * @return mixed
     */
    public function resolveRoute(string $route)
    {
        if (array_key_exists($route, $this->routes)) {
            $controller_name = $this->routes[$route]['controller'];
            $controller_class = new $controller_name();
            $action_name = $this->routes[$route]['action'];

            return $controller_class->{$action_name}();
        } else {
            return '404 Страница не найдена';
        }
    }
    
    public function generateRoutes()
    {
        $this->addRoute('/', [
            'controller' => IndexController::class,
            'action' => 'index',
        ]);

        $this->addRoute('/question/create', [
            'controller' => QuestionController::class,
            'action' => 'create',
        ]);

        $this->addRoute('/admin', [
            'controller' => AdminController::class,
            'action' => 'index',
        ]);

        $this->addRoute('/admin/login', [
            'controller' => AdminController::class,
            'action' => 'login',
        ]);

        $this->addRoute('/admin/logout', [
            'controller' => AdminController::class,
            'action' => 'logout',
        ]);

        $this->addRoute('/admin/user/list', [
            'controller' => AdminControllerr::class,
            'action' => 'adminUserList',
        ]);

        $this->addRoute('/admin/user/delete', [
            'controller' => AdminController::class,
            'action' => 'adminUserDelete',
        ]);

        $this->addRoute('/admin/user/create', [
            'controller' => AdminController::class,
            'action' => 'adminUserCreate',
        ]);

        $this->addRoute('/admin/user/edit', [
            'controller' => AdminController::class,
            'action' => 'adminUserEdit',
        ]);

        $this->addRoute('/admin/user/change_password', [
            'controller' => AdminController::class,
            'action' => 'adminChangePassword',
        ]);

        $this->addRoute('/admin/category/list', [
            'controller' => AdminCategoryController::class,
            'action' => 'list',
        ]);

        $this->addRoute('/admin/category/create', [
            'controller' => AdminCategoryController::class,
            'action' => 'create',
        ]);

        $this->addRoute('/admin/category/delete', [
            'controller' => AdminCategoryController::class,
            'action' => 'delete',
        ]);

        $this->addRoute('/admin/question/list', [
            'controller' => AdminQuestionController::class,
            'action' => 'list',
        ]);

        $this->addRoute('/admin/question/answer', [
            'controller' => AdminQuestionController::class,
            'action' => 'answer',
        ]);

        $this->addRoute('/admin/question/publish', [
            'controller' => AdminQuestionController::class,
            'action' => 'publish',
        ]);

        $this->addRoute('/admin/question/hide', [
            'controller' => AdminQuestionController::class,
            'action' => 'hide',
        ]);

        $this->addRoute('/admin/question/change_category', [
            'controller' => AdminQuestionController::class,
            'action' => 'changeCategory',
        ]);

        $this->addRoute('/admin/question/edit', [
            'controller' => AdminQuestionController::class,
            'action' => 'edit',
        ]);
    }
}
