<?php

namespace App\Controller;

use App\DependencyInjection\Di;
use App\Entity\Category;
use App\Manager\QuestionManager;

class QuestionController
{
    public function create()
    {
        if (array_key_exists('username', $_POST) && array_key_exists('useremail', $_POST) && array_key_exists('question', $_POST)) {
            $questionmng = new QuestionManager();
            $questionmng->createNewQuestion($_POST['username'], $_POST['useremail'], $_POST['category'], $_POST['question']);
            echo "Вопрос отправлен на модерацию";
        }

        return Di::get()->templater('question/create_question.html.twig', [
            'categories' => (new Category())->findAll(),
        ]);
    }
}


