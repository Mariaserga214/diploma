<?php

namespace App\Controller;

use App\DependencyInjection\Di;
use App\Entity\Question;

class IndexController
{
    public function index()
    {
        $questions = (new Question())->findPublished();

        return Di::get()->templater('index/index.html.twig', [
            'questions' => $questions,
        ]);
    }

    public function something()
    {
        return Di::get()->templater('index/index.html.twig', [
            'name' => 'something',
        ]);
    }

    public function anything()
    {
        $_GET['param1'];
        $_POST['param2'];

        return 'here we are';
    }

    public function jsonresponse()
    {
        header('Content-type: text/json');

        return json_encode(['something']);
    }
}
