<?php

namespace App\Controller;

use App\DependencyInjection\Di;
use App\Entity\Category;
use App\Manager\CategoryManager;
use App\Manager\SessionManager;

class AdminCategoryController
{
    protected $session_manager;

    public function __construct()
    {
        $this->session_manager = new SessionManager();
    }

    protected function checkAccess()
    {
        if (!$this->session_manager->isUserLogged()) {
            header('Location: /admin/login');
        }
    }

    public function list()
    {
        $this->checkAccess();

        $categories = (new Category())->findAll();

        return Di::get()->templater('admin/category/list.html.twig', [
            'categories' => $categories,
        ]);
    }

    public function create()
    {
        $this->checkAccess();

        if (array_key_exists('name', $_POST)) {
            $category_manager = new CategoryManager();
            $category_manager->createCategory($_POST['name']);

            header('Location: /admin/category/list');
        }

        return Di::get()->templater('admin/category/form.html.twig', []);
    }

    public function delete()
    {
        $this->checkAccess();

        if (array_key_exists('id', $_GET)) {
            header('Location: /admin/category/list');
        }

        $category = (new Category())->findById($_GET['id']);

        $category->delete();

        header('Location: /admin/category/list');
    }
}
