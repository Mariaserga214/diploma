<?php

namespace App\Controller;

use App\DependencyInjection\Di;
use App\Entity\User;
use App\Manager\SessionManager;
use App\Manager\UserManager;

class AdminController
{
    protected $session_manager;

    public function __construct()
    {
        $this->session_manager = new SessionManager();
    }

    protected function checkAccess()
    {
        if (!$this->session_manager->isUserLogged()) {
            header('Location: /admin/login');
        }
    }

    public function index()
    {
        $this->checkAccess();

        return Di::get()->templater('admin/index.html.twig', []);
    }

    public function login()
    {
        if ($this->session_manager->isUserLogged()) {
            header('Location: /admin');
        }

        $message = '';

        if (array_key_exists('login', $_POST) && array_key_exists('password', $_POST)) {
            $admin = (new User())->findAdminWithEmailAndPassword($_POST['login'], $_POST['password']);

            if ($admin) {
                $this->session_manager->loginUser($admin);
                header('Location: /admin');
            } else {
                $message = 'Неверный пароль';
            }
        }

        return Di::get()->templater('admin/login.html.twig', [
            'message' => $message,
        ]);
    }

    public function logout()
    {
        $this->session_manager->logoutUser();

        header('Location: /admin');
    }

    public function adminUserList()
    {
        $this->checkAccess();

        return Di::get()->templater('admin/user/list.html.twig', [
            'users' => (new User())->findAdmins(),
        ]);
    }

    public function adminUserCreate()
    {
        $this->checkAccess();

        if (array_key_exists('email', $_POST) && array_key_exists('name', $_POST) && array_key_exists('password', $_POST)) {
            $user_manager = new UserManager();

            if (array_key_exists('is_admin', $_POST) && $_POST['is_admin']) {
                $user_manager->createNewAdmin($_POST['email'], $_POST['name'], $_POST['password']);
            } else {
                $user_manager->createNewUser($_POST['email'], $_POST['name'], $_POST['password']);
            }

            header('Location: /admin/user/list');
        }

        return Di::get()->templater('admin/user/form.html.twig', [
            'user' => null,
        ]);
    }

    public function adminUserDelete()
    {
        $this->checkAccess();

        if (!array_key_exists('id', $_GET)) {
            header('Location: /admin/user/list');
        }

        $user = (new User())->findById($_GET['id']);
        $user->delete();

        header('Location: /admin/user/list');
    }

    public function adminUserEdit()
    {
        $this->checkAccess();

        if (!array_key_exists('id', $_GET)) {
            header('Location: /admin/user/list');
        }

        $user = (new User())->findById($_GET['id']);

        if (array_key_exists('email', $_POST) && array_key_exists('name', $_POST)) {
            $user->setName($_POST['name']);
            $user->setIsAdmin($_POST['is_admin'] ?? false);
            $user->setEmail($_POST['email']);

            $user->update();

            header('Location: /admin/user/list');
        }

        return Di::get()->templater('admin/user/form.html.twig', [
            'user' => $user,
        ]);
    }

    public function adminChangePassword()
    {
        $this->checkAccess();

        if (!array_key_exists('id', $_GET)) {
            header('Location: /admin/user/list');
        }

        $user = (new User())->findById($_GET['id']);

        if (array_key_exists('password', $_POST)) {
            $user_manager = new UserManager();
            $user_manager->changeAdminPassword($user, $_POST['password']);

            header('Location: /admin/user/list');
        }

        return Di::get()->templater('admin/user/change_password.html.twig', [
            'user' => $user,
        ]);
    }
}
