<?php

namespace App\Controller;

use App\DependencyInjection\Di;
use App\Entity\Category;
use App\Entity\Question;
use App\Entity\User;
use App\Manager\QuestionManager;
use App\Manager\SessionManager;

class AdminQuestionController
{
    protected $question_manager;
    protected $session_manager;

    public function __construct()
    {
        $this->session_manager = new SessionManager();
        $this->question_manager = new QuestionManager();
    }

    protected function checkAccess()
    {
        if (!$this->session_manager->isUserLogged()) {
            header('Location: /admin/login');
        }
    }

    public function list()
    {
        $this->checkAccess();

        $questions = (new Question())->findAllByCategory($_GET['category_id'] ?? null);

        return Di::get()->templater('admin/question/list.html.twig', [
            'questions' => $questions,
        ]);
    }

    public function answer()
    {
        $this->checkAccess();

        if (!array_key_exists('id', $_GET)) {
            header('Location: /admin/question/list');
        }

        $question = (new Question())->findById($_GET['id']);

        if (array_key_exists('answer', $_POST)) {
            $this->question_manager->answerToQuestion($question, $_POST['answer']);

            header('Location: /admin/question/list');
        }

        return Di::get()->templater('admin/question/answer.html.twig', [
            'question' => $question,
        ]);
    }

    public function publish()
    {
        $this->checkAccess();

        if (!array_key_exists('id', $_GET)) {
            header('Location: /admin/question/list');
        }

        $question = (new Question())->findById($_GET['id']);

        $this->question_manager->publishQuestion($question);

        header('Location: /admin/question/list');
    }

    public function hide()
    {
        $this->checkAccess();

        if (!array_key_exists('id', $_GET)) {
            header('Location: /admin/question/list');
        }

        $question = (new Question())->findById($_GET['id']);

        $this->question_manager->hideQuestion($question);

        header('Location: /admin/question/list');
    }

    public function delete()
    {
        $this->checkAccess();

        if (!array_key_exists('id', $_GET)) {
            header('Location: /admin/question/list');
        }

        $question = (new Question())->findById($_GET['id']);

        $this->question_manager->deleteQuestion($question);

        header('Location: /admin/question/list');
    }

    public function changeCategory()
    {
        $this->checkAccess();

        if (!array_key_exists('id', $_GET)) {
            header('Location: /admin/question/list');
        }

        $question = (new Question())->findById($_GET['id']);
        $categories = (new Category())->findAll();

        if (array_key_exists('category', $_POST)) {
            $this->question_manager->changeCategory($question, (new Category())->findById($_POST['category']));

            header('Location: /admin/question/list');
        }

        return Di::get()->templater('admin/question/change_category.html.twig', [
            'question' => $question,
            'categories' => $categories,
        ]);
    }

    public function edit()
    {
        $this->checkAccess();

        if (!array_key_exists('id', $_GET)) {
            header('Location: /admin/question/list');
        }

        $question = (new Question())->findById($_GET['id']);
        $categories = (new Category())->findAll();
        $users = (new User())->findAll();

        if (array_key_exists('text', $_POST) && array_key_exists('answer', $_POST) && array_key_exists('user', $_POST) && array_key_exists('category', $_POST)) {
            $this->question_manager->editQuestion(
                $question,
                (new User())->findById($_POST['user']),
                (new Category())->findById($_POST['category']),
                $_POST['text'],
                $_POST['answer']
            );

            header('Location: /admin/question/list');
        }

        return Di::get()->templater('admin/question/form.html.twig', [
            'question' => $question,
            'users' => $users,
            'categories' => $categories,
        ]);
    }
}
