<?php

namespace App\DependencyInjection;

class Di
{
    static $di = null;

    public static function get()
    {
        if (!self::$di) {
            self::$di = new Di();
        }

        return self::$di;
    }

    public function config()
    {
        $config = include __DIR__.'/../Config/Config.php';

        return $config;
    }

    public function db()
    {
        $config = $this->config()['mysql'];

        try {
            $db = new \PDO(
                'mysql:host='.$config['host'].';dbname='.$config['dbname'].';charset=utf8',
                $config['user'],
                $config['pass']
            );
            $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            die('Database error: '.$e->getMessage().'<br/>');
        }

        return $db;
    }

    public function templater(string $filename, array $params)
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__.'/../../templates');
        $twig = new \Twig_Environment($loader, array(
//            'cache' => __DIR__.'/../../cache/tpl/',
        ));

        $user = [];

        if (isset($_SESSION) && array_key_exists('user', $_SESSION)) {
            $user = [
                'app_user' => $_SESSION['user']
            ];
        }

        $params = array_merge($params, $user);

        return $twig->render($filename, $params);
    }
}
