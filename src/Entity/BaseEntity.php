<?php

namespace App\Entity;

use App\DependencyInjection\Di;

abstract class BaseEntity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $created_at;

    /**
     * @var \DateTime
     */
    protected $updated_at;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return BaseEntity
     */
    public function setId(int $id): BaseEntity
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     * @return BaseEntity
     */
    public function setCreatedAt(\DateTime $created_at): BaseEntity
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updated_at;
    }

    /**
     * @param \DateTime $updated_at
     * @return BaseEntity
     */
    public function setUpdatedAt(\DateTime $updated_at): BaseEntity
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function delete()
    {
        $sql = Di::get()->db()->prepare(sprintf('DELETE FROM %s WHERE id = :id', $this->getTableName()));
        $sql->bindValue('id', $this->id, \PDO::PARAM_INT);
        $sql->execute();
    }

    public function findById(int $id)
    {
        $sql = Di::get()->db()->prepare(sprintf('SELECT * FROM %s WHERE id = :id', $this->getTableName()));
        $sql->bindValue('id', $id, \PDO::PARAM_INT);
        $sql->execute();

        $data = $sql->fetchAll(\PDO::FETCH_ASSOC)[0];

        return $this->createFromArray($data);
    }

    public function findAll()
    {
        $sql = Di::get()->db()->prepare(sprintf('SELECT * FROM %s', $this->getTableName()));
        $sql->execute();

        $data = $sql->fetchAll(\PDO::FETCH_ASSOC);

        $result = [];

        foreach ($data as $info) {
            $result[] = $this->createFromArray($info);
        }

        return $result;
    }

    abstract public function getTableName();

    abstract public function createFromArray(array $data);
}
