<?php

namespace App\Entity;

use App\DependencyInjection\Di;

class User extends BaseEntity
{
    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var bool
     */
    protected $is_admin = false;

    public function __construct()
    {
        $this->created_at = new \DateTime('now');
        $this->updated_at = new \DateTime('now');
    }

    public function create()
    {
        $sql = Di::get()->db()->prepare('INSERT INTO user (email, password, name, is_admin, created_at, updated_at) VALUES (:email, :password, :name, :is_admin, :created_at, :updated_at)');
        $sql->bindValue('email', $this->email);
        $sql->bindValue('password', $this->password);
        $sql->bindValue('name', $this->name);
        $sql->bindValue('is_admin', (int)$this->is_admin);
        $sql->bindValue('created_at', $this->created_at->format('Y-m-d H:i:s'));
        $sql->bindValue('updated_at', $this->updated_at->format('Y-m-d H:i:s'));
        $sql->execute();

        /* @todo: после вставки юзера в базу, нужно его получить из базы и вернуть */

        return $this;
    }

    public function update()
    {
        $sql = Di::get()->db()->prepare('UPDATE user SET email = :email, password = :password, name = :name, is_admin = :is_admin, updated_at = :updated_at WHERE id = :id');
        $sql->bindValue('id', $this->id);
        $sql->bindValue('email', $this->email);
        $sql->bindValue('password', $this->password);
        $sql->bindValue('name', $this->name);
        $sql->bindValue('is_admin', $this->is_admin);
        $sql->bindValue('updated_at', $this->updated_at->format('Y-m-d H:i:s'));
        $sql->execute();

        return $this->findById($this->id);
    }

    public function findByNameAndEmail(string $name, string $email)
    {
        $sql = Di::get()->db()->prepare('SELECT * FROM user WHERE LOWER(name) = LOWER(:name) AND LOWER(email) = LOWER(:email)');
        $sql->bindValue('name', $name);
        $sql->bindValue('email', $email);
        $sql->execute();

        $user_data = $sql->fetchAll(\PDO::FETCH_ASSOC);

        if ($user_data) {
            return $this->createFromArray($user_data[0]);
        }

        $user = new User();
        $user->setEmail($email);
        $user->setName($name);
        $user->setPassword('');
        $user->setIsAdmin(false);

        return $user->create();
    }

    public function getTableName()
    {
        return 'user';
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->is_admin;
    }

    /**
     * @param bool $is_admin
     * @return User
     */
    public function setIsAdmin(bool $is_admin): User
    {
        $this->is_admin = $is_admin;

        return $this;
    }

    public function createFromArray(array $data)
    {
        $user = new static();
        $user->setId($data['id']);
        $user->setName($data['name']);
        $user->setEmail($data['email']);
        $user->setPassword($data['password']);
        $user->setIsAdmin($data['is_admin']);
        $user->setCreatedAt(new \DateTime($data['created_at']));
        $user->setUpdatedAt(new \DateTime($data['updated_at']));

        return $user;
    }

    /**
     * @param string $email
     * @param string $password
     * @return $this|null
     */
    public function findAdminWithEmailAndPassword(string $email, string $password)
    {
        $sql = Di::get()->db()->prepare('SELECT * FROM user WHERE LOWER(email) = LOWER(:email) AND password = :password AND is_admin = 1');
        $sql->bindValue('email', $email);
        $sql->bindValue('password', $password);
        $sql->execute();

        $user_data = $sql->fetchAll(\PDO::FETCH_ASSOC);

        if ($user_data) {
            $this->createFromArray($user_data[0]);

            return $this;
        }

        return null;
    }

    public function findAdmins()
    {
        $sql = Di::get()->db()->prepare('SELECT * FROM user WHERE is_admin = 1');
        $sql->execute();

        return $sql->fetchAll(\PDO::FETCH_ASSOC);
    }
}
