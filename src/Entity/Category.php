<?php

namespace App\Entity;

use App\DependencyInjection\Di;

class Category extends BaseEntity
{
    /**
     * @var string
     */
    protected $name;

    public function __construct()
    {
        $this->created_at = new \DateTime('now');
        $this->updated_at = new \DateTime('now');
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName(string $name): Category
    {
        $this->name = $name;

        return $this;
    }

    public function getTableName()
    {
        return 'category';
    }

    public function findById(int $id)
    {
        $sql = Di::get()->db()->prepare('SELECT * FROM category WHERE id = :id');
        $sql->bindValue('id', $id);
        $sql->execute();

        $data = $sql->fetchAll(\PDO::FETCH_ASSOC)[0];

        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->created_at = $data['created_at'];
        $this->updated_at = $data['updated_at'];

        return $this;
    }

    public function countActiveQuestions()
    {
        $sql = Di::get()->db()->prepare('SELECT COUNT(*) FROM question WHERE category_id = :category_id AND status = :status');
        $sql->bindValue('category_id', $this->id);
        $sql->bindValue('status', Question::STATUS_PUBLISHED);
        $sql->execute();

        return $sql->fetchColumn();
    }

    public function countWaitingQuestions()
    {
        $sql = Di::get()->db()->prepare('SELECT COUNT(*) FROM question WHERE category_id = :category_id AND status = :status');
        $sql->bindValue('category_id', $this->id);
        $sql->bindValue('status', Question::STATUS_WAITING);
        $sql->execute();

        return $sql->fetchColumn();
    }

    public function countHiddenQuestions()
    {
        $sql = Di::get()->db()->prepare('SELECT COUNT(*) FROM question WHERE category_id = :category_id AND status = :status');
        $sql->bindValue('category_id', $this->id);
        $sql->bindValue('status', Question::STATUS_HIDDEN);
        $sql->execute();

        return $sql->fetchColumn();
    }

    public function createFromArray(array $data)
    {
        $category = new static();
        $category->setId($data['id']);
        $category->setCreatedAt(new \DateTime($data['created_at']));
        $category->setUpdatedAt(new \DateTime($data['updated_at']));
        $category->setName($data['name']);

        return $category;
    }

    public function create()
    {
        $sql = Di::get()->db()->prepare('INSERT INTO category (name, created_at, updated_at) VALUES (:name, :created_at, :updated_at)');
        $sql->bindValue('name', $this->name);
        $sql->bindValue('created_at', $this->created_at->format('Y-m-d H:i:s'));
        $sql->bindValue('updated_at', $this->updated_at->format('Y-m-d H:i:s'));
        $sql->execute();

        return $this;
    }

    public function delete()
    {
        $sql = Di::get()->db()->prepare('DELETE FROM question WHERE category_id = :category_id');
        $sql->bindValue('id', $this->id, \PDO::PARAM_INT);
        $sql->execute();

        $sql = Di::get()->db()->prepare(sprintf('DELETE FROM %s WHERE id = :id', $this->getTableName()));
        $sql->bindValue('id', $this->id, \PDO::PARAM_INT);
        $sql->execute();
    }
}
