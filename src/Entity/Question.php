<?php

namespace App\Entity;

use App\DependencyInjection\Di;

class Question extends BaseEntity
{
    public const STATUS_WAITING = 1;
    public const STATUS_PUBLISHED = 2;
    public const STATUS_HIDDEN = 3;

    protected const STATUS_MAP = [
        self::STATUS_WAITING => 'В ожидании',
        self::STATUS_PUBLISHED => 'Опубликован',
        self::STATUS_HIDDEN => 'Скрыт',
    ];

    /**
     * Текст вопроса
     *
     * @var string
     */
    protected $text;

    /**
     * Ответ на вопросы
     *
     * @var string
     */
    protected $answer;

    /**
     * @var int
     */
    protected $status = self::STATUS_WAITING;

    /**
     * @var Category
     */
    protected $category;

    /**
     * @var User
     */
    protected $user;

    public function __construct()
    {
        $this->created_at = new \DateTime('now');
        $this->updated_at = new \DateTime('now');
    }

    public function getTableName()
    {
        return 'question';
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return Question
     */
    public function setText(string $text): Question
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     * @return Question
     */
    public function setAnswer($answer): Question
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Question
     */
    public function setStatus(int $status): Question
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Question
     */
    public function setCategory(Category $category): Question
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Question
     */
    public function setUser(User $user): Question
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return $this
     *
     * @todo: реализовать создание объекта Question
     */
    public function create()
    {
        $sql = Di::get()->db()->prepare('INSERT INTO question (text, answer, status, category_id, user_id, created_at, updated_at) VALUES (:text, :answer, :status, :category_id, :user_id, :created_at, :updated_at)');
        $sql->bindValue('text', $this->text);
        $sql->bindValue('answer', $this->answer);
        $sql->bindValue('status', $this->status);
        $sql->bindValue('category_id', $this->category->getId());
        $sql->bindValue('user_id', $this->user->getId());
        $sql->bindValue('created_at', $this->created_at->format('Y-m-d H:i:s'));
        $sql->bindValue('updated_at', $this->updated_at->format('Y-m-d H:i:s'));
        $sql->execute();

        /* @todo: после вставки юзера в базу, нужно его получить из базы и вернуть */

        return $this;
    }

    /**
     * @return $this
     *
     * @todo: реализовать обновление объекта Question
     */
    public function update()
    {
        $sql = Di::get()->db()->prepare('UPDATE question SET text = :text, answer = :answer, status = :status, user_id = :user_id, category_id = :category_id, updated_at = :updated_at WHERE id = :id');
        $sql->bindValue('id', $this->id);
        $sql->bindValue('text', $this->text);
        $sql->bindValue('answer', $this->answer);
        $sql->bindValue('status', $this->status);
        $sql->bindValue('category_id', $this->category->getId());
        $sql->bindValue('user_id', $this->user->getId());
        $sql->bindValue('updated_at', (new \DateTime('now'))->format('Y-m-d H:i:s'));
        $sql->execute();

        return $this->findById($this->id);
    }

    public function createFromArray(array $data)
    {
        $question = new static();
        $question->setId($data['id']);
        $question->setUser((new User())->findById($data['user_id']));
        $question->setStatus($data['status']);
        $question->setCreatedAt(new \DateTime($data['created_at']));
        $question->setUpdatedAt(new \DateTime($data['updated_at']));
        $question->setText($data['text']);
        $question->setAnswer($data['answer']);
        $question->setCategory((new Category())->findById($data['category_id']));

        return $question;
    }

    public function findPublished()
    {
        $sql = Di::get()->db()->prepare('SELECT * FROM question WHERE status = :status ORDER BY updated_at DESC');
        $sql->bindValue('status', static::STATUS_PUBLISHED);
        $sql->execute();

        $data = $sql->fetchAll(\PDO::FETCH_ASSOC);

        $result = [];

        foreach ($data as $info) {
            $result[] = $this->createFromArray($info);
        }

        return $result;
    }

    public function findAllByCategory($category_id)
    {
        if ($category_id) {
            $sql = Di::get()->db()->prepare('SELECT * FROM question WHERE category_id = :category_id ORDER BY created_at DESC');
            $sql->bindValue('category_id', $category_id);
        } else {
            $sql = Di::get()->db()->prepare('SELECT * FROM question ORDER BY created_at DESC');
        }

        $sql->execute();

        $data = $sql->fetchAll(\PDO::FETCH_ASSOC);

        $result = [];

        foreach ($data as $info) {
            $result[] = $this->createFromArray($info);
        }

        return $result;
    }

    public function getStatusName()
    {
        return static::STATUS_MAP[$this->status];
    }
}
