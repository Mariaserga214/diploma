<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use App\Manager\Router;

require_once __DIR__.'/../vendor/autoload.php';

$router = new Router();

$router->generateRoutes();

echo $router->resolveRoute($_SERVER['DOCUMENT_URI']);
