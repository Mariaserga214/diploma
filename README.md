### Настройка NGINX

```
server {
    listen 80;
    server_name {YOUR_SERVER_NAME};
    set $root_path {PATH_TO_DIRECTORY_WITH_PROJECT};

    root $root_path/web;

    location / {
      try_files $uri @app;
    }

    location @app {
      include fastcgi_params;
      fastcgi_param SCRIPT_FILENAME $root_path/web/index.php;
      fastcgi_param SCRIPT_NAME /index.php;
      fastcgi_pass 127.0.0.1:9000;
    }

    error_log {PATH_TO_NGINX_LOG_DIR}/error.log;
    access_log {PATH_TO_NGINX_LOG_DIR}/netology.access.log;
}
```

### Первый запуск

Создать локально базу данных с именем netology.
При необходимости скорректировать файл src/Config/Config.php для подключения к базе данных.
Загрузить в новую базу данных файл faq.sql.
Данные администратора - логин: admin@admin, пароль: 123



