CREATE TABLE category (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    created_at DATETIME,
    updated_at DATETIME,
    PRIMARY KEY (id)
);

CREATE TABLE question (
    id INT NOT NULL AUTO_INCREMENT,
    text TEXT,
    answer TEXT,
    status INT,
    category_id INT,
    user_id INT,
    created_at DATETIME,
    updated_at DATETIME,
    PRIMARY KEY (id)
);

CREATE TABLE user (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(255),
    password VARCHAR(255),
    name VARCHAR(255),
    is_admin INT(1),
    created_at DATETIME,
    updated_at DATETIME,
    PRIMARY KEY (id)
);

ALTER TABLE question ADD INDEX question_user (user_id);
ALTER TABLE question ADD INDEX question_category (category_id);

ALTER TABLE question ADD CONSTRAINT question_ibfk_1 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE question ADD CONSTRAINT question_ibfk_2 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO category (name, created_at, updated_at) VALUES ('PHP', NOW(), NOW());
INSERT INTO category (name, created_at, updated_at) VALUES ('HTML', NOW(), NOW());
INSERT INTO category (name, created_at, updated_at) VALUES ('SQL', NOW(), NOW());

INSERT INTO user (email, password, name, is_admin, created_at, updated_at) VALUES ('admin@admin', '123', 'Admin', 1, NOW(), NOW());
